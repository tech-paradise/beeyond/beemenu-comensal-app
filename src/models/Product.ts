import Price from './Price'
import Ingredient from './Ingredient'
import LocalizedText from './LocalizedText'

export default class Product {
    id: string
    active = true
    categoryId = ''
    subCategoryId: string | undefined = undefined
    chefRecommended = false
    chefRecommendedPriority = 0
    description = ''
    mostSelled = true
    mostSelledPriority = 0
    name = ''
    photos: Array<string> = []
    order = 0
    delivery = false
    video = ''

    //Calculated
    photosURL: Array<string> = []    
    goodReviews= 0
    badReviews= 0
    totalReviews = 0    

    //Sub-Collections
    prices: Price[] = []
    ingredients: Ingredient[] = []
    localizedNames: LocalizedText[] = []
    localizedDescriptions: LocalizedText[] = []
    
    // Reviews Module

    constructor(id = '') {
        this.id = id
    }

}
