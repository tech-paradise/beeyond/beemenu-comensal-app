export default class InvoiceData {

    id: string
    idUser = ''
    name = ''
    email = ''
    phoneNumber = ''
    rfc = ''
    businessName = ''
    address = ''
    extNumber = ''
    intNumber = ''
    postalCode = ''
    state = ''
    locality = ''
    region = ''

    constructor(id: string){
        this.id = id
    }
}