import Address from './Address'
import Item from './Item'
import { PaymentDetails } from './PaymentDetails'

export enum StatusDelivery {
    ORDER = 'ORDER',
    LOCATION = 'LOCATION',
    SUMMARY = 'SUMMARY',
    REDIRECTED = 'REDIRECTED',
    FINISHED = 'FINISHED'
}

export enum PaymentMethod {
    CARD = "CARD",
    CASH = "CASH",
    TRANSFER = "TRANSFER",
    ONLINE = "ONLINE"
}

export default class Order{
    id = ""
    restaurantId: string
    menuId = ''
    items: Array<Item> = []
    name ='' 
    userId = ''
    sent = false
    subtotal = 0
    total = 0
    deliveryCost = 0
    type = ''
    status = ''
    currency = ''
    number = ''
    createdAt = ''
    udpatedAt = ''
    payment: PaymentMethod = PaymentMethod.CASH 
    paymentDetails?: PaymentDetails | undefined = undefined
    cash?: string = ''
    location?: string = ''
    phone?: string = ''
    phoneExt?: string = ''
    email?: string = ''
    address?: Address | undefined = undefined
    statusDelivery?: string = StatusDelivery.ORDER
    constructor(restaurantId = ''){
        this.restaurantId = restaurantId
    }
}