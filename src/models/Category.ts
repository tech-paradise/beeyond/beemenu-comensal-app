import LocalizedText from './LocalizedText'
import Subcategory from './Subcategory'

export default class Category {
    id: string
    active = true
    icon = ""
    iconURL = ""
    name = ""
    nameEnglish = ""   
    showsAtHome = false
    showsAtHomePriority = 0
    order = 0
    isEmpty = false
    localizedNames: Array<LocalizedText> = []
    subcategories: Array<Subcategory> = []
    
    constructor(id = '') {
        this.id = id
    }
}
