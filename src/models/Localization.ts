
export default class Localization{
    id: string 
    name = ""
    language = ""
    currency = ""
    change = ""
    icon = ""
    iconURL = ""
    order = 0
    constructor(id = ''){
        this.id = id
    }
}
