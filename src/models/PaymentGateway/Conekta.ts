import { TokenParams } from '@/models/PaymentGateway/TokenParams';

export interface Conekta {
	Card: {
		validateNumber(number: string): boolean
		validateExpirationDate(month: string | number, year: string | number): boolean
		validateCVC(cvc: string | number): boolean
		getBrand(number: string): string | unknown
	}

	Token: {
		create(tokenParams: TokenParams, successResponseHandler: Function, errorResponseHandler: Function)
	}
	    
    setPublicKey: Function
	setLanguage: Function
	getPublicKey: Function
	getLanguage: Function
}

export interface ConektaCheckoutComponents {
	Integration: Function;
	iFrame: Function;
	PaymentLink: Function;
	Card: Function;
	createToken: Function;
	Cash: Function;
	BankTransfer: Function;
}
