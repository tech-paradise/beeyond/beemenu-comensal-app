export enum ApiErrors {
    // Payment
    NOT_FINALIZED = "Operation couldn't be completed",
    INVALID_EMAIL = 'Invalid Email',
    INVALID_USERNAME = 'User have a invalid user name',
    INVALID_PHONE = 'Invalid phone',
    TYPE_PAYMENT_GATEWAY = 'Invalid type payment gateway',
    TYPE_CHECKOUT_GATEWAY = 'Invalid type checkout gateway',
    TYPE_GATEWAY_EXIST = 'User has this payment gateway',
    USER_GATEWAY_NOT_EXIST = "User doesn't has this payment gateway",
    PAYMENT_SOURCE_EXIST = 'The card has already been registered',
    INVALID_CARD_TOKEN = 'Invalid token',
    ORDER_NOT_YOURS = 'This order does not belong to you',
    INCORRECT_CARD = 'Incorrect Card',
    ORDER_HAS_PAYMENT = 'Payment order exist',
    INVALID_LATITUDE = 'Invalid Latitude',
    INVALID_LONGITUDE = 'Invalid Longitude',
    INVALID_START_DATE = 'Invalid Start Date',
    INVALID_END_DATE = 'Invalid End Date',
 
    //Errors payment
    PAY_ERROR_BLOCK_FOR_BANK = 'do_not_honor', //'Your bank has blocked your payment',
    PAY_ERROR_INCORRECT_CVC = 'incorrect_cvc', //'The cvv is wrong',
    PAY_ERROR_INSUFFICIENT_FOUNDS = 'insufficient_funds', //'Your card does not have enough fund',
    PAY_ERROR_STOLEN_CARD = 'stolen_card',
    PAY_ERROR_LOST_CARD = 'lost_card',
    PAY_ERROR_CARD_DECLINED = 'card_declined',
    PAY_ERROR_EXPIRED_CARD = 'expired_card',
    PAY_ERROR_PROCESSING_ERROR = 'processing_error',
    PAY_ERROR_INCORRECT_NUMBER = 'incorrect_number',
    PAY_ERROR_RATE_LIMIT_EXCEEDED = 'card_decline_rate_limit_exceeded',
 
    //Favorite
    INVALID_ORDER_PLACED = 'Order placed must be a number',
 
    //Adress
    ADDR_INVALID_ID = 'Address must have a id',
    ADDR_INVALID_LOCATION = 'Address must have location',
    ADDR_INVALID_FORMAT = 'Address must have formatted address',
    ADDR_INVALID_DESCRIPTION = 'This address type need a description',
}