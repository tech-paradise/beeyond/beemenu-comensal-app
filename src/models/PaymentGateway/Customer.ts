export default class Customer {
    constructor(
        public id: string,
        public name: string,
        public email: string,
        public object: string,
        public createdAt: string
    ) {}
}