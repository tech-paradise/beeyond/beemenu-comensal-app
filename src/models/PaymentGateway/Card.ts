import Address from './Address'

export default class Card {
	number = ''
	name = ''
	exp_year = ''
	exp_month = ''
	cvc = ''
	address? = new Address()
}
