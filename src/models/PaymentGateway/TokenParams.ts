import Card from './Card'

export class TokenParams {
    card = new Card()
}

export class Token {
	id = ''
	object = ''
    used = false
    livemode = false
}

export class Error {
	type = ''
	message = ''
    message_to_purchaser = ''
    error_code = ''
    param = ''
}

export enum StatusError {
    CARD_DECLINED = 'card_declined',
    INCORRECT_NUMBER = 'incorrect_number',
    INVALID_EXPIRY_MONTH = 'invalid_expiry_month',
    INVALID_EXPIRY_YEAR = 'invalid_expiry_year',
    INVALID_CVC = 'invalid_cvc'
}