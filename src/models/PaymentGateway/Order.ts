export class CustomerInfo {
    constructor(
        public email?: string,
        public name?: string,
        public phone?: string,
    ) {}
}

export class Checkout {
    constructor(public allowed_payment_methods: string[], public type: string, public success_url?: string, public failure_url?: string) {}
}

export class LineItem {
    public constructor(
        public name?: string,
        public unit_price?: number,
        public quantity?: number,
    ) {}
}

export class Order {
    public constructor(
        public checkout?: Checkout,
        public line_items?: LineItem[],
        public created_at?: number,
        public currency?: string,
        public livemode?: boolean,
        public customer_info?: CustomerInfo,
    ) {}

    static empty(): Order {
        return new Order(new Checkout([], ''), [], 0, '', false, new CustomerInfo());
    }
} 