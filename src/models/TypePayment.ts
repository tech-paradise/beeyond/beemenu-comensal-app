export enum TypePayment {
    CONEKTA = 'CONEKTA',
    CASH = 'CASH',
    ND = 'ND',
    STRIPE = 'STRIPE'
}