import ItemExtraIngredient from "./ItemExtraIngredient"

export default class ItemIngredient{
    id = ""
    localizedNames = ""
    selected = false
    extraIngredients: Array<ItemExtraIngredient> = []
}