import ItemIngredient from './ItemIngredient'
import Price from './Price'

export default class Item { 
    id = ""
    name = ""
    category = ""
    subcategory? = ""
    photoURL = ""
    quantity = 0
    price: Price | undefined = undefined
    priceIngredients = 0
    ingredients: Array<ItemIngredient> = []
    details = ""
    localizedName = ''
    type = ''
}