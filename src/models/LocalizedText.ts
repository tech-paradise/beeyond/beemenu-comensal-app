export default class LocalizedText{
    id: string | null = null
    text = ""
    language = ""
    constructor(id = ''){
        this.id = id
    }
}