export default class BusinessCategory { 
    id: string
    name = ''
    active = false

    constructor(id = '') {
		this.id = id
	}
}