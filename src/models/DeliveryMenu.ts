import firebase from "firebase/app";
import Timestamp = firebase.firestore.Timestamp;
import LocalizedText from "@/models/LocalizedText";

export enum DaysOfWeek{
    LUNES='LUNES',
    MARTES='MARTES',
    MIERCOLES='MIERCOLES',
    JUEVES='JUEVES',
    VIERNES='VIERNES',
    SABADO='SABADO',
    DOMINGO='DOMINGO'
}

export type Schedule = {
    [key in DaysOfWeek]: {
        initDay: DaysOfWeek;
        initHour: Date;
        endDay: DaysOfWeek;
        endHour: Date;
        open: boolean;
    }
}

export type ScheduleFromFB = {
    [key in DaysOfWeek]: {
        initDay: string;
        initHour: Timestamp;
        endDay: string;
        endHour: Timestamp;
        open: boolean;
    }
}

export enum DistanceRadius {
    RADIUS1 = 'radius1',
    RADIUS2 = 'radius2',
    RADIUS3 = 'radius3',
    RADIUS4 = 'radius4'
}

export type CostShipping = {
    [key in DistanceRadius]: {
        cost: number;
        distance: number;
    };
};
export interface Payments{
    cash: boolean;
    online: {
        active: boolean;
        conekta: boolean;
        stripe: boolean;
    };
    terminal: {
        active: boolean;
        americanExpress: boolean;
        mastercard: boolean;
        visa: boolean;
    };
    transfer: {
        active?: boolean;
        bankAccount?: string;
        beneficiary?: string;
        clabe?: string;
        concept?: string;
        nameBank?: string;
        numberCard?: string;
    };
}
export interface Phones{
    localPhone: {
        phone: string;
    };
    whatsapp: {
        message: string;
        phone: string;
    };
}

export interface Location{
    lat: number;
    lng: number;
}

export class DeliveryMenu{
    id ='';
    address = '';
    costShipping: CostShipping | undefined = undefined;
    active = false;
    descriptionDeliveryDefault = '';
    imageDelivery = '';
    imageDeliveryURL =  '';
    location: Location | undefined = undefined;
    menuId = '';
    onlinePayment = false
    payment: Payments | undefined = undefined;
    phones: Phones | undefined = undefined;
    restaurantId = '';
    schedule: Schedule | undefined = undefined;
    shipping = false;
    collect = false;

    descriptionDelivery: LocalizedText[] = []
    descriptionEstimates: LocalizedText[] = []

    constructor(id: string) {
        this.id = id
    }
}