import LocalizedText from './LocalizedText'
import ExtraIngredient from './ExtraIngredient'

export default class Ingredient{
   id: string 
   name = ''
   maxAmount = 0
   productId = ''
   required = false
   extraIngredients: Array<ExtraIngredient> = []
   localizedNames: Array<LocalizedText> = []
   selected = false
   isEmpty = false
   constructor(id: string){
       this.id = id
   }
}