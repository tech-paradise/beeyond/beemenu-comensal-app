import Price from "./Price"

export default class ItemExtraIngredient{
    id = ""
    localizedNames = ""
    price: Price | undefined = undefined
    selected = false
} 