import firebase from "firebase";
export default class Address { 
    number?: string
    street?: string
    region?: string
    locality?: string
    state?: string
    country?: string
    postal?: string
    location?: firebase.firestore.GeoPoint
    extNumber?: string
    references?: string
}