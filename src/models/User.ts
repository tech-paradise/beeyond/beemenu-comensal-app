export class User {

    constructor(
        public readonly id: string, 
        public readonly name: string, 
        public email: string, 
        public phone: string,
        public authToken?: string
        ) {
    }

}