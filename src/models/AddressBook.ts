export default class AddressBook {
	id?: string
	defaultAddress: boolean
	location: Location
	formatAddress: string
	country: string
	locality: string
	state: string
	street: string
	postal: string
	references: string
	type: TypeAddressBook
	otherDescription?: string

	constructor(
		id: string,
		defaultAddress: boolean,
		location: Location,
		formatAddress: string,
		country: string,
		state: string,
		street: string,
		postal: string,
		type: TypeAddressBook,
		locality: string,
		references: string,
		otherDescription: string
	) {
		(this.id = id),
		(this.defaultAddress = defaultAddress),
		(this.location = location),
		(this.formatAddress = formatAddress),
		(this.country = country),
		(this.locality = locality),
		(this.state = state),
		(this.street = street),
		(this.postal = postal),
		(this.references = references),
		(this.type = type),
		(this.otherDescription = otherDescription)
	}
}

export class Location {
	constructor(readonly lat: number, readonly lng: number) {}
	static empty(): Location {
		return new Location(0, 0)
	}
}

export enum TypeAddressBook {
	HOME = 'HOME',
	OFFICE = 'OFFICE',
	PARENT = 'PARENT',
	OTHER = 'OTHER'
}

export function typeAddressBookFromString(text: string): TypeAddressBook {
	const upper = text.trim().toUpperCase()
	const enumValues = Object.values(TypeAddressBook)
	const e = enumValues.find(val => val === upper)
	return e ? e : TypeAddressBook.OTHER
}
