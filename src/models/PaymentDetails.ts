import { TypePayment } from '@/models/TypePayment'

export enum PaymentStatus {
    PENDING_PAYMENT = "PENDING_PAYMENT",
    DECLINED = "DECLINED",
    EXPIRED = "EXPIRED",
    PAID = "PAID",
    REFUNDED = "REFUNDED",
    PARTIALLY_REFUNDED = "PARTIALLY_REFUNDED",
    CHARGED_BACK = "CHARGED_BACK",
    PRED_AUTHORIZED = "PRED_AUTHORIZED",
    VOIDED  = "VOIDED",
    UNKNOWN = "UNKNOWN"
}

export class PaymentDetails {
    status: PaymentStatus = PaymentStatus.UNKNOWN
    amount = 0
    orderIdGateway = ''
    type: TypePayment = TypePayment.ND
    totalDeposit = 0
    authBank = ''
    fee = 0
    cardBank = ''
    cardBrand = ''
    cardName = ''
}