import LocalizedText from './LocalizedText'

export default class Subcategory {
    id: string
    active = true
    icon = ""
    iconURL = ""
    name = ""
    order = 0
    isEmpty = false
    localizedNames: Array<LocalizedText> = []
    
    constructor(id = '') {
        this.id = id
    }
}
