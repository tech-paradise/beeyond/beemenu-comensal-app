export class Slug {

    public id: string;
    public menusSlugs: Map<string,string>;
    public restaurantId: string;

    constructor(id?: string,restaurantId?: string,menusSlugs?: Map<string,string>) {
        this.id=id || '';
        this.restaurantId = restaurantId || '';
        this.menusSlugs= menusSlugs || new Map<string, string>();
    }
}
