import Product from './Product'
import Category from './Category'
import Localization from './Localization'
import Ingredient from './Ingredient'

export enum BusinessType {
    ND = 'ND',
	RESTAURANTS = 'RESTAURANTES',
	GOURMET = 'GOURMET',
	SERVICES = 'SERVICIOS',
	PHARMACY = 'FARMACIAS',
	HOTEL = 'HOTEL',
	EXPRESS = 'EXPRESS'
}

export enum TypeView {
    SHIPPING = 'SHIPPING',
    COLLECT = 'COLLECT',
    QR = 'QR'
}

export enum InfoTag {
	ND = 'ND',
	PROX = 'PROXIMAMENTE',
	FREESHIPPING = '♥ ENVIOS GRATIS ♥',
	CLOSE = 'CERRADO',
	DISCOUNT15 = 'DESCUENTO 15%',
	DISCOUNT20 = 'DESCUENTO 20%',
	DISCOUNT30 = 'DESCUENTO 30%',
	DISCOUNT40 = 'DESCUENTO 40%',
	DISCOUNT50 = 'DESCUENTO 50%',
	PROMOCOMBO = 'PROMOCION EN COMBO',
	SHIPPING1 = 'Envios a $1'
}
export interface InfoBusiness {
	id: string;
	restaurantId: string;
	name: string;
	description: string;
	logo: string | null;
	saving?: number;
	directory?: {
		businessType: BusinessType;
		infoTag?: InfoTag;
		businessCategory: Array<string>;
	};
	open: boolean;
	openAt?: string;
}

export default class Menu {
	id: string
	restaurantId: string | null = null
	background: string | null = null
	logo: string | null = null
	description = ''
	mainColor = '#feca38'
	buttonTextColor = '#000000'
	showChefRecommended = true
	showMostSelled = true
	showReviews = false
	name = ''
	active = false
	useLogin = false
	localizations: Array<Localization> = []
	products: Array<Product> = []
	categories: Array<Category> = []
	ingredients: Array<Ingredient> = []


	constructor(id: string) {
		this.id = id
	}
}
