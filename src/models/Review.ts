export default class Review {
    id: string
    content = ''
    language = ''
    name = '' 
    positive = true
    createdAt = new Date () 
    
    constructor(id = ''){
        this.id = id
    }
}
