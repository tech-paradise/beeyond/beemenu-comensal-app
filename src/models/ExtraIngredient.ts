import LocalizedText from './LocalizedText'
import Price from './Price'

export default class ExtraIngredient{
   id: string 
   name = ''

   localizedNames: Array<LocalizedText> = []
   prices: Price[] = []
   selected = false
   constructor(id: string){
       this.id = id
   }
}