import Vue from 'vue'
import Vuex from 'vuex'
import StorageService from '@/services/StorageService'
import InitialState, { MainView, TypeOrder } from './state'
import Restaurant from '@/models/Restaurant'
import {getRestaurantById, getSlugById, getSlugByRestaurantId} from '@/services/RestaurantService'
import MenuService from '@/services/MenuService'
import Menu from '@/models/Menu'
import i18n from '@/plugins/i18n'
import Order from '@/models/Order'
import {User} from '@/models/User'
import Item from '@/models/Item'
import OrderService from '@/services/OrderService'
import createdPersistedState from 'vuex-persist'
import {Slug} from "@/models/Slug";
import Product from '@/models/Product'
import Address from '@/models/Address'
import {DeliveryMenu} from "@/models/DeliveryMenu";
import {getDeliveryMenu} from "@/services/DeliveryMenuService";
import AddressBook from "@/models/AddressBook";
import AddressService from '@/services/AddressService'

const vuexPersist = new createdPersistedState({ 
	key: 'BeeMenu',
	storage: window.localStorage
})

Vue.use(Vuex)
export default new Vuex.Store({
	plugins: [vuexPersist.plugin],
	state: InitialState,
	mutations: {
		setRestaurant(state, restaurant: Restaurant) {
			state.restaurant = restaurant
		},

		setMenu(state, menu: Menu) {
			state.menu = menu
		},

		setLanguage(state, newLanguage: string) {
			state.language = newLanguage
		},

		setCurrency(state, newCurrency: string) {
			state.currency = newCurrency
			if (state.order) state.order.currency = newCurrency
		},

		setCustomLogoURL(state, url: string) {
			state.customLogoURL = url
		},

		setCustomBackgroundURL(state, url: string) {
			state.customBackgroundURL = url
		},

		setOrder(state, newOrder: Order | null) {
			state.order = newOrder
		},

		setUser(state, newUser: User | null) {
			state.user = newUser
		},
		setBook(state, newAddressBook: AddressBook[] | undefined) {
			state.addressBook = newAddressBook
		},
		setSlug(state, newSlug: Slug | undefined){
			state.slug = newSlug;
		},

		setTypeOrder(state, typeOrder: TypeOrder){
			state.typeOrder = typeOrder
		},

		setAddress(state, newAddress: Address){
			state.address = newAddress
		},

		setBookMapAddress(state, newAddress: AddressBook){
			state.mapAddress = newAddress
		},

		setDeliveryMenu(state, deliveryMenu: DeliveryMenu){
			state.deliveryMenu = deliveryMenu
		},

		setCategoryId(state, categoryId: string){
			state.categoryId = categoryId
		},

		setMainView(state, mainView: MainView){
			state.mainView = mainView
		}
	},
	actions: {

		async getRestaurant(context, restaurantOrSlugId: string) {
			try {
				await context.dispatch('getSlugById', restaurantOrSlugId);

				if(context.state.slug){
					restaurantOrSlugId = context.state.slug.restaurantId;
				}
				const restaurant = await getRestaurantById(restaurantOrSlugId);

				await context.commit('setRestaurant', restaurant);
			} catch (error) {
				console.log('Error looking for restaurante', error);
			}
		},

		async getMenu(context, idOrSlug: string) {
			if (context.state.restaurant) {
				let menu: Menu | null = null;

				if (!context.state.slug){
					await context.dispatch('getSlugByRestaurantId',context.state.restaurant.id)
				}
				const menusSlugs = context.state.slug?.menusSlugs || new Map<string, string>();

				if(!idOrSlug){
					idOrSlug = await MenuService.getMenuIdFromRestaurant(context.state.restaurant.id) || '';
				}

				const slugToMenuId = menusSlugs.get(idOrSlug);

				if(slugToMenuId){
					idOrSlug = slugToMenuId;
				}

				menu = await MenuService.getMenu(idOrSlug)
				if (menu && menu.restaurantId != context.state.restaurant.id) {
					menu = null
				}
				context.commit('setMenu', menu)

				await Promise.all([context.dispatch('getLogoURL'), context.dispatch('getBackgroundURL')])
			}
		},

		async setMainView(context, mainView: MainView){
			await context.commit('setMainView', mainView)
		},

		async setTypeOrder(context, typeOrder: TypeOrder){
			await context.commit('setTypeOrder', typeOrder)
		},

		async setAddress(context, newAddress: Address){
			await context.commit('setAddress', newAddress)
		},
		
		async setMapAddress(context, newAddress: AddressBook){
			await context.commit('setBookMapAddress', newAddress)
		},

		async setCategoryId(context, categoryId: string){
			await context.commit('setCategoryId', categoryId)
		},

		async getSlugById(context, idSlug: string){
			const slug = await getSlugById(idSlug);
			await context.commit('setSlug', slug);
		},

		async getSlugByRestaurantId(context, restaurantId: string){
			const slug = await getSlugByRestaurantId(restaurantId);
			await context.commit('setSlug', slug);
		},

		async getDeliveryMenu(context, menuId){
			const deliveryMenu = await getDeliveryMenu(menuId);
			await context.commit('setDeliveryMenu', deliveryMenu);
		},

		async setNewOrder(context) {
			const newOrder = new Order()
			newOrder.userId = context.state.user ? context.state.user.id : ''
			newOrder.menuId = context.state.menu ? context.state.menu.id : ''
			newOrder.currency = context.state.currency ? context.state.currency : ''
			newOrder.restaurantId = context.state.restaurant ? context.state.restaurant.id : ''
			newOrder.sent = false
			context.commit('setOrder', newOrder)
		},

		async login(context, user: User | null) {
			context.commit('setUser', user)
		},

		async addItemToOrder(context, item: Item) {
			if (context.state.order && !context.state.order.sent) {
				const updatedOrder = context.state.order
				if(item.price != undefined){
					updatedOrder.subtotal += (item.price.ammount + item.priceIngredients) * item.quantity
				}
				const index = OrderService.getIndexOfItem(item, updatedOrder.items)
				if (index == -1) {
					updatedOrder.items.push(item)
				} else {
					updatedOrder.items[index].quantity += item.quantity
				}
				context.commit('setOrder', updatedOrder)
			}
		},

		async updateItemQuantity(context, { index, quantity }) {
			if (context.state.order && !context.state.order.sent) {
				const updatedOrder = context.state.order
				if (quantity == 0) {
					updatedOrder.items.splice(index, 1)
				} else {
					updatedOrder.items[index].quantity = quantity
				}
				updatedOrder.subtotal = updatedOrder.items.reduce(function(total, item) {
					if(item.price?.ammount){
						return total + (item.quantity * item.price.ammount) + (item.priceIngredients * item.quantity)	
					}else return updatedOrder.subtotal
				}, 0)
				context.commit('setOrder', updatedOrder)
			}
		},

		async getLogoURL(context) {
			const url = await StorageService.getFile(context.state.menu?.logo || '')
			this.commit('setCustomLogoURL', url || '')
		},

		async getBackgroundURL(context) {
			const url = await StorageService.getFile(context.state.menu?.background || '')
			this.commit('setCustomBackgroundURL', url || '')
		},

		changeLanguage({ commit }, newLanguage: string) {
			i18n.locale = newLanguage
			commit('setLanguage', newLanguage)
		},

		changeCurrency({ commit }, newCurrency: string) {
			commit('setCurrency', newCurrency)
		}
	},
	modules: {},
	getters: {

		resid: state => {
			if (!state.restaurant) return false
			return state.restaurant.id
		},

		deliverystatus: state => {
			if (!state.deliveryMenu) return false
			return state.deliveryMenu.shipping
		},
		
		colors: (state): { backgroundColor: string; textColor: string } => {
			if (state.menu == null) return { backgroundColor: '#feca38', textColor: '#000' }
			return { backgroundColor: state.menu.mainColor, textColor: state.menu.buttonTextColor }
		},

		canOrder: state => {
			if (!state.restaurant) return false
			return state.restaurant.receiveOrders
		},

		showRecommended: state => {
			if (!state.menu) return false
			return state.menu.showChefRecommended
		},

		showMostSelled: state => {
			if (!state.menu) return false
			return state.menu.showMostSelled
		},

		products: state => {
			if (!state.menu) return []
			if (state.typeOrder == TypeOrder.DELIVERY) return state.menu.products.filter(p => p.delivery)
			return state.menu.products
		},

		addressBook: state => (addressId: string) => {
			if (!state.addressBook) return []
			return state.addressBook.find(address => address.id == addressId)
		},

		categories: state => {
			if (!state.menu) return []
			if(state.typeOrder == TypeOrder.DELIVERY){
				const prods = state.menu.products.filter(p => p.delivery) as Product[];
				state.menu.categories.forEach(async category => {
					let count = 0;
					for(let i = 0; i < prods.length; i++) {
						if(category.id == prods[i].categoryId){
							count += 1
							break;
						}
					}
					if(count === 0) category.isEmpty = true
				})
				return state.menu.categories.filter(c => !c.isEmpty)
			}
			return state.menu.categories
		},

		subcategories: state => {
			if (!state.menu) return []
			return state.menu.categories
				.find( category => category.id == state.categoryId)
				?.subcategories
		},

		ingredients: (state) => (productId: string) => {
			if (!state.menu) return []
			return state.menu.ingredients.filter(i => i.productId == productId)
		}
	}
})
