import Restaurant from '@/models/Restaurant';
import Menu from '@/models/Menu';
import Order from '@/models/Order';
import {User} from "@/models/User";
import {Slug} from "@/models/Slug";
import Address from '@/models/Address';
import {DeliveryMenu} from "@/models/DeliveryMenu";
import AddressBook from "@/models/AddressBook";

export enum TypeOrder {
    LOCAL = 'LOCAL',
    DELIVERY = 'DELIVERY',
    COLLECT = 'COLLECT'
}

export enum MainView {
    DIRECTORY = 'DIRECTORY',
    MENU = 'MENU',
    COMMAND = 'COMMAND'
}

interface State {
    restaurant: Restaurant | null;
    menu: Menu | null;
    language: string;
    currency: string;
    categoryId: string;
    customLogoURL: string;
    customBackgroundURL: string;
    order: Order | null;
    user: User | null;
    slug: Slug | undefined;
    typeOrder: TypeOrder;
    address: Address | null;
    deliveryMenu: DeliveryMenu | undefined;
    addressBook: AddressBook[] | undefined;
    mapAddress: AddressBook | undefined;
    mainView: MainView | undefined;
}

export default {
    restaurant: null,
    menu: null,
    language: 'spanish',
    customLogoURL: '',
    customBackgroundURL: '',
    currency: '',
    categoryId: '',
    order: null,
    user: null,
    slug: undefined,
    typeOrder: TypeOrder.LOCAL,
    address: null,
    deliveryMenu: undefined,
    addressBook: undefined,
    mainView: undefined
} as State 
