import Vue from 'vue';
import firebase from 'firebase';
import VueCardFormat from 'vue-credit-card-validation';
declare module 'vue/types/vue' {
    interface Vue {
        $cardFormat: VueCardFormat;
        $analytics : firebase.analytics.Analytics;
    }
    interface VueConstructor {
        $cardFormat: VueCardFormat;
        $analytics : firebase.analytics.Analytics;
    }
}

declare module 'vue/types/options' {
    interface ComponentOptions<V extends Vue> {
        $cardFormat?: VueCardFormat;
        $analytics? : firebase.analytics.Analytics;
    }
}