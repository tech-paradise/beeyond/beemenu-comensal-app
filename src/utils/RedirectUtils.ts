import router from "@/router"
import store from "@/store"
import { MainView } from "@/store/state"

export function redirect() {
    switch(store.state.mainView){
        case(MainView.DIRECTORY):
            router.push('/')
            break
        
        case(MainView.MENU):
            router.push('/menu')
            break
            
        case(MainView.COMMAND):
            router.push('/order')
            break
    }
}