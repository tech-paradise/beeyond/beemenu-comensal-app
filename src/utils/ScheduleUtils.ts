import {DaysOfWeek} from "@/models/DeliveryMenu";
import axios from "axios";

export function getDayOfWeek(day: number){
    const days: DaysOfWeek[] = [
        DaysOfWeek.DOMINGO,
        DaysOfWeek.LUNES,
        DaysOfWeek.MARTES,
        DaysOfWeek.MIERCOLES,
        DaysOfWeek.JUEVES,
        DaysOfWeek.VIERNES,
        DaysOfWeek.SABADO
    ]
    return days[day];
}

export async function isOpen(menuId: string): Promise<boolean> {
    try {
        const result = await axios.get(`${process.env.VUE_APP_CATALOG}/${menuId}/open`)
        return result.data.open
    } catch (error) {
        console.log(error)
    }
    return false;
}