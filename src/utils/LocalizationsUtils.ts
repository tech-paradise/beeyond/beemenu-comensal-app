import LocalizedText from "@/models/LocalizedText";
import store from "@/store";
import {DeliveryMenu} from "@/models/DeliveryMenu";

export function localizedDeliveryDescription(delivery: DeliveryMenu): string {
    const name: LocalizedText | undefined = delivery.descriptionDelivery.find(localizedNames => localizedNames.language == store.state.language)
    if (name != undefined) { return name.text }

    return delivery.descriptionDeliveryDefault;
}

export function localizedDeliveryEstimate(delivery: DeliveryMenu): string {
    const name: LocalizedText | undefined = delivery.descriptionEstimates.find(localizedNames => localizedNames.language == store.state.language)
    if (name != undefined) { return name.text }

    return '';
}

