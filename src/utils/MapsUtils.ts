import { DistanceRadius } from "@/models/DeliveryMenu";
import store from "@/store"

export async function parseAddress(data, ref: string){
  const typeData = {
    number: ["street_number"],
    street: ["street_address", "route"],
    region: [
      "political",
      "sublocality",
      "sublocality_level_1",
      "sublocality_level_2",
      "sublocality_level_3",
      "sublocality_level_4"
    ],
    locality: ["locality"],
    state: [
      "administrative_area_level_1",
      "administrative_area_level_2",
      "administrative_area_level_3",
      "administrative_area_level_4",
      "administrative_area_level_5"
    ],
    country: ["country"],
    postal: ["postal_code"],
  };

  let res = ''

  data.address_components.forEach(component => {
    for (const type in typeData) {
      if (typeData[type].indexOf(component.types[0]) !== -1) {
        if (type === ref) {
          res = component.long_name;
        }
      }
    }
  })
  return res
}

function deg2rad(deg){
  return deg * (Math.PI/180)
}

function haversineFormula(locationUser: firebase.firestore.GeoPoint, locationCatalog: any){
  const R = 6371
  const dLat: number = deg2rad(locationUser.latitude-locationCatalog.lat);
  const dLon: number = deg2rad(locationUser.longitude-locationCatalog.lng); 
  const a: number = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(locationCatalog.lat)) * Math.cos(deg2rad(locationUser.latitude)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2) 
  const c: number = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
  return R * c * 1000
}

function getRange(distance: number){
  if(store.state.deliveryMenu?.costShipping && store.state.order){
    const radius1: number = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS1].distance
    const radius2: number = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS2].distance
    const radius3: number = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS3].distance
    const radius4: number = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS4].distance
    if(!distance){
      store.state.order.deliveryCost = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS1].cost
    }
    else if(distance <= radius1 && distance >= 0){
      console.log(store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS1].cost)
      store.state.order.deliveryCost = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS1].cost
      return true
    }else if(distance <= radius2){
      store.state.order.deliveryCost = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS2].cost
      return true
    }else if(distance <= radius3){
      store.state.order.deliveryCost = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS3].cost
      return true
    }else if(distance <= radius4){
      store.state.order.deliveryCost = store.state.deliveryMenu.costShipping[DistanceRadius.RADIUS4].cost
      return true
    }
    return false
  }
  return false
}

export async function getDeliveryCost(locationUser: firebase.firestore.GeoPoint, locationCatalog: any){
  const distance: number = haversineFormula(locationUser, locationCatalog)
  const inRange: boolean = getRange(distance)
  return inRange
}