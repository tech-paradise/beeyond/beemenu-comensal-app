import Firebase from './firebase'
import firebase from 'firebase/app'
import store from '../store'
import { getUser } from '@/services/UserService'
import Analytics from './analytics'

export default Firebase.auth()
export const GProvider = new firebase.auth.GoogleAuthProvider()
export const FBProvider = new firebase.auth.FacebookAuthProvider()

export const startAuthStateChanged = () => {
	Firebase.auth().onAuthStateChanged(async authUser => {
		if (authUser) {
			const user = await getUser(authUser)
			Analytics.setUserId(authUser.uid)
			await store.dispatch('login', user)
		} else {
			await store.dispatch('login', null)
		}
	})
}
