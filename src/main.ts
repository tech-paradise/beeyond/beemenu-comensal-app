import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import MenuLocalization from './mixins/MenuLocalization'
import i18n from './plugins/i18n'
import { VueReCaptcha } from 'vue-recaptcha-v3'
import VueGeolocation from 'vue-browser-geolocation';
import VueLazyload from 'vue-lazyload'
import {startAuthStateChanged} from "@/config/auth";
import VueCardFormat from 'vue-credit-card-validation';
import Analytics from '@/config/analytics'

startAuthStateChanged();
Vue.mixin(MenuLocalization)

if (process.env.NODE_ENV == 'production') {
	try {
		Vue.use(VueReCaptcha, { siteKey: process.env.VUE_APP_RECAPTCHA })
	} catch (e) {
		console.log(e)
	}
}

Vue.config.productionTip = false
Vue.use(VueGeolocation);
Vue.use(VueLazyload);

Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: 'dist/error.png',
	loading: 'dist/loading.gif',
	attempt: 1
})

Vue.prototype.$analytics = Analytics;
Vue.prototype.$cardFormat = VueCardFormat
Vue.use(VueCardFormat);

new Vue({
	router,
	store,
	i18n,
	render: h => h(App)
}).$mount('#app')
