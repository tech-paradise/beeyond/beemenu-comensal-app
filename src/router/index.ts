import { TypeOrder } from '@/store/state'
import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '../store/index'
import Analytics from '@/config/analytics'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
	{
		path: '/',
		name: 'BeeMarket',
		component: () => import('../views/BeeMarket.vue')
	},
	{
		path: '/restaurants/:restaurantId/menus/:menuId',
		name: 'Splashscreen with menu',
		component: () => import('../views/Splashscreen.vue')
	},
	{
		path: '/restaurants/:restaurantId',
		name: 'Splashscreen without menu',
		component: () => import('../views/Splashscreen.vue')
	},
	{
		path: '/restaurants/:restaurantId/menus/:menuId/delivery/:origin?',
		name: 'Delivery',
		component: () => import('../views/InfoRestaurant.vue')
	},
	{
		path: '/menu',
		name: 'Menu',
		component: () => import('../views/Menu.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/categories',
		name: 'Categories',
		component: () => import('../views/Categories.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/subcategories',
		name: 'Subcategories',
		component: () => import('../views/Subcategories.vue'),
		meta: {
			requiresFoundMenu: true
		},
		props: true
	},
	{
		path: '/categories/:categoryId',
		name: 'Category',
		component: () => import('../views/Category.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/subcategories/:subcategoryId',
		name: 'Subcategory',
		component: () => import('../views/Subcategory.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/products/:productId',
		name: 'Product',
		component: () => import('../views/Dish.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/reviews/:productId',
		name: 'Reviews',
		component: () => import('../views/Reviews.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/order',
		name: 'order',
		component: () => import('../views/Command.vue'),
		meta: {
			requiresFoundMenu: true
		}
	},
	{
		path: '/maps',
		name: 'Maps',
		component: () => import('../views/Maps.vue'),
		meta: {
			requiresOrderOrUser: true
		}
	},
	{
		path: '/deliveryform',
		name: 'DeliveryForm',
		component: () => import('../views/DeliveryForm.vue'),
		meta: {
			requiresOrder: true
		}
	},
	{
		path: '/summary',
		name: 'SummaryDelivery',
		component: () => import('../views/SummaryDelivery.vue'),
		meta: {
			requiresOrder: true
		}
	},
	{
		path: '/login',
		name: 'Login',
		component: () => import('../views/Login.vue'),
		meta: {
			requiresUser: false
		}
	},
	{
		path: '/directions',
		name: 'Directions',
		component: () => import('../views/Directions.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/profile',
		name: 'Profile',
		component: () => import('../views/Profile.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/PersonalInfo',
		name: 'PersonalInfo',
		component: () => import('../views/PersonalInfo.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/PersonalInfoEdit',
		name: 'PersonalInfoEdit',
		component: () => import('../views/PersonalInfoEdit.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/ChangePassword',
		name: 'ChangePassword',
		component: () => import('../views/ChangePassword.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/ConnectSocialPlatforms',
		name: 'ConnectSocialPlatforms',
		component: () => import('../views/ConnectSocialPlatforms.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/WrongPassword',
		name: 'WrongPassword',
		component: () => import('../views/WrongPassword.vue'),
		meta: {
			requiresUser: false
		}
	},
	{
		path: '/InvoiceRegister',
		name: 'InvoiceRegister',
		component: () => import('../views/InvoiceRegister.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/myShopping',
		name: 'myShopping',
		component: () => import('../views/myShopping.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/newPassword',
		name: 'newPassword',
		component: () => import('../views/newPassword.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/SummaryDeliveryConfirm',
		name: 'SummaryDeliveryConfirm',
		props: route => ({
			checkoutId: route.query.checkout_id,
			orderId: route.query.order_id,
			paymentStatus: route.query.payment_status
		}),
		component: () => import('../views/SummaryDeliveryConfirm.vue')
	},
	{
		path: '/Help',
		name: 'Help',
		component: () => import('../views/Help.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/Favorites',
		name: 'favorites',
		component: () => import('../views/Favorites.vue'),
		meta: {
			requiresUser: true
		}
	},
	{
		path: '/locations',
		name: 'locations',
		component: () => import('../views/Locations.vue'),
	},
	{
		path: '/location',
		name: 'location',
		component: () => import('../views/Location.vue'),
		props: true
	},
	{
		path: '/:restaurantId/:menuId/:origin?',
		name: 'Delivery with menu',
		component: () => import('../views/Delivery.vue')
	},
	{
		path: '/:restaurantId/:origin?',
		name: 'Delivery without menu',
		component: () => import('../views/Delivery.vue')
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		console.log('this Page needs logged user')
	}

	if (to.matched.some(record => record.meta.requiresFoundMenu == true)) {
		if (store.state.restaurant == null || store.state.menu == null) {
			next({ path: '/menu' })
		}
	}

	if (to.matched.some(record => record.meta.requiresOrder == true)) {
		const paths = ['/order', '/maps', '/deliveryform', '/summary', '/locations', '/location']
		if (!paths.includes(from.path) || store.state.typeOrder != TypeOrder.DELIVERY) {
			next({ path: '/menu' })
		}
	}

	if (to.matched.some(record => record.meta.requiresUser == false)) {
		if (store.state.user != undefined || null) {
			next({ path: '/menu' })
		}
	}

	if (to.matched.some(record => record.meta.requiresUser == true)) {
		if (store.state.user == undefined || null) {
			next({ path: '/menu' })
		}
	}

	if (to.fullPath === '/' || to.fullPath.includes('/restaurants')) {
		window.document.title = 'BeeMenu'
	} else {
		window.document.title = store.state.menu ? store.state.menu.name : 'BeeMenu'
	}

	Analytics.logEvent('page_view', {
		page_title: window.document.title,
		page_path: to.path,
		page_location: window.location.href
	})

	next()
})

export default router
