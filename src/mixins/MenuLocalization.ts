import Vue from "vue";
import store from "@/store/index";
import Product from "@/models/Product";
import Category from "@/models/Category";
import LocalizedText from '@/models/LocalizedText';
import Ingredient from '@/models/Ingredient';
import Price from '@/models/Price';
import ExtraIngredient from "@/models/ExtraIngredient";
import Localization from '@/models/Localization';

export function localizedCategoryName(category: Category): string {
    const name: LocalizedText | undefined = category.localizedNames.find(localizedNames => localizedNames.language == store.state.language)
    if (name != undefined) { return name.text }

    return category.name
}

export function localizedProductName(product: Product): string {
    const name: LocalizedText | undefined = product.localizedNames.find(localizedNames => localizedNames.language == store.state.language)
    if (name != undefined) { return name.text }

    return product.name
}

export function localizedProductDescription(product: Product): string {
    const name: LocalizedText | undefined = product.localizedDescriptions.find(localizedDescription => localizedDescription.language == store.state.language)
    if (name != undefined) { 
        return name.text 
    }

    return product.description
}

export function localizedProductPrice(product: Product): Price | null {
    if(store.state.language){
        const localizations = store.state.menu?.localizations as Array<Localization>
        const local = localizations.find(local => local.language == store.state.language)
        const price: Price | undefined = product.prices.find(price => price.currency == local?.currency)
        if(price != undefined) return price 
    }
    return null
}


export function localizedExtraIngredientPrice(extraIngredient: ExtraIngredient): Price | null {
    if(store.state.menu){
        const localizations = store.state.menu.localizations as Array<Localization>
        const local = localizations.find(local => local.language == store.state.language)
        const price: Price | undefined = extraIngredient.prices.find(price => price.currency == local?.currency)
        if(price != undefined) return price
    }
    return null
}

export function localizedExtraIngredientName(extraIngredient: ExtraIngredient): string {
    const name: LocalizedText | undefined = extraIngredient.localizedNames.find(l => l.language == store.state.language)
    if (name != undefined) { return name.text }

    return extraIngredient.name
}

export function localizedIngredientName(ingredient: Ingredient): string {
    const name: LocalizedText | undefined = ingredient.localizedNames.find(l => l.language == store.state.language)
    if (name != undefined) { return name.text }

    return ingredient.name
}

export function formatPrice(price: Price | number, showCurrency = false): string {
    const priceToFormat = typeof(price) === 'object' ? price.ammount.toFixed(2) : price.toFixed(2)

    if(priceToFormat === "0.00") return ''
    
    //https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
    const formatedPrice =  priceToFormat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 

    return `$${formatedPrice} ${showCurrency? store.state.currency : ''}`
}

export default Vue.extend({
    methods: {
        localizedCategoryName,
        localizedProductName,
        localizedProductDescription,
        localizedIngredientName,
        localizedExtraIngredientName,
        localizedProductPrice,
        localizedExtraIngredientPrice,
        formatPrice
    }
})
