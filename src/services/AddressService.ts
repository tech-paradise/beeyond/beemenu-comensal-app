import auth from '@/config/auth'
import AddressBook from "@/models/AddressBook";
import axios from "axios";

export async function saveAddressBook(addressBook: AddressBook) {
    if (auth.currentUser) {
        const options = {
            headers: {
                Authorization: await auth.currentUser.getIdToken(true)
            }
        }
        try {
            const request = await axios.post(`${process.env.VUE_APP_ADDRESS}`, addressBook, options)
            return request
        } catch (err) {
            console.log(err)
        }
    }
}

export async function getAllAddressBook() {
    if (auth.currentUser) {
        const options = {
            headers: {
                Authorization: await auth.currentUser.getIdToken(true)
            }
        }
        try {
            const request = await axios.get(`${process.env.VUE_APP_ADDRESS}`, options)
            return request
        } catch (err) {
            console.log(err)
        }
    }
}

export async function getAddressBookById(idAddress: string){
    if (auth.currentUser) {
        const options = {
            headers: {
                Authorization: await auth.currentUser.getIdToken(true)
            }
        }
        try {
            const request = await axios.get(`${process.env.VUE_APP_ADDRESS}/${idAddress}`, options)
            return request
        } catch (err) {
            console.log(err)
        }
    }
}

export async function updateAddressBook(idAddress: string, addressBook: AddressBook){
    if (auth.currentUser) {
        const options = {
            headers: {
                Authorization: await auth.currentUser.getIdToken(true)
            }
        }
        try {
            const request = await axios.put(`${process.env.VUE_APP_ADDRESS}/${idAddress}`, addressBook, options)
            return request
        } catch (err) {
            console.log(err)
        }
    }
}

export async function deleteAddress(idAddress: string) {
    if (auth.currentUser) {
        const options = {
            headers: {
                Authorization: await auth.currentUser.getIdToken(true)
            }
        }
        try {
            const request = await axios.delete(`${process.env.VUE_APP_ADDRESS}/${idAddress}`, options)
            return request
        } catch (err) {
            console.log(err)
        }
    }
}

export default{
    getAddressBookById,
    getAllAddressBook,
    saveAddressBook,
    updateAddressBook,
    deleteAddress
}
