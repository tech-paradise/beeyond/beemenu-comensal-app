import db from "@/config/db";
import Order from '@/models/Order';
import firebase from 'firebase';
import Item from '@/models/Item';
import ItemIngredient from '@/models/ItemIngredient';
import {ORDERS, ITEMS, INGREDIENTS, EXTRA_INGREDIENTS, PAYMENTS, USER, FAVORITES} from './CollectionsNames';
import { TypeOrder } from '@/store/state';
import ItemExtraIngredient from '@/models/ItemExtraIngredient';
import store from "@/store";

interface OrderObject {
    restaurantId: string;
    menuId: string;
    name: string;
    userId: string;
    subtotal: number;
    total: number;
    type: string;
    status: string;
    currency: string;
    readableId: string;
    createdAt: firebase.firestore.Timestamp;
    updatedAt: firebase.firestore.Timestamp;
    captchaToken: string;
    [key: string]: any;
}

function getRandomInt(min = 0, max = 99): number {
    return Math.floor(Math.random() * (max - min)) + min;
}

async function paymentExist(orderId: string): Promise<boolean> {
    const payment = await db.collection(PAYMENTS).where('metadata.orderId', '==', orderId).get();
    if (payment.empty) return false
    return true
}

function pad(n = 0, size = 2) {
    let s = String(n);
    while (s.length < (size)) { s = "0" + s; }
    return s;
}

async function saveOrder(order: Order, token = "", type: string, costService?: number) {

    const batch = db.batch()
    const newOrder = db.collection(ORDERS).doc()

    const date = new Date()
    const orderObject: OrderObject = {
        restaurantId: order.restaurantId,
        menuId: order.menuId,
        name: order.name,
        userId: order.userId,
        subtotal: order.subtotal,
        total: order.total,
        type: order.type,
        status: order.status,
        currency: order.currency,
        readableId: order.number,
        createdAt: new firebase.firestore.Timestamp(date.getTime()/1000, 0),
        updatedAt: new firebase.firestore.Timestamp(date.getTime()/1000, 0),
        captchaToken: token
    }

    if(type == TypeOrder.LOCAL){
        orderObject.location = order.location
        batch.set(newOrder, orderObject)
    }else if(type == TypeOrder.COLLECT){
        orderObject.phone = order.phone
        orderObject.email = order.email
        batch.set(newOrder, orderObject)
    }else if(type == TypeOrder.DELIVERY){
        orderObject.phone = order.phone
        orderObject.phoneExt = order.phoneExt
        orderObject.email = order.email
        orderObject.methodPayment = order.payment
        orderObject.cashInfo = order.cash
        orderObject.address = order.address
        if(order.paymentDetails) orderObject.detailsPayment = order.paymentDetails
        batch.set(newOrder, orderObject)
    }

    for (const item of order.items) {
        const itemDoc = await newOrder.collection(ITEMS).doc()
        const nItem = {
            id: item.id,
            name: item.name,
            category: item.category,
            subcategory: item.subcategory,
            quantity: item.quantity,
            price: item.price?.ammount,
            details: item.details,
            localizedName: item.localizedName,
            type: item.type
        }
        batch.set(itemDoc, nItem)

        for (const ingredient of item.ingredients) {
            const ingredientDoc = itemDoc.collection(INGREDIENTS).doc()
            const nItemIngredient = {
                id: ingredient.id,
                localizedName: ingredient.localizedNames,
                selected: ingredient.selected,
            }
            batch.set(ingredientDoc, nItemIngredient)
            for (const extraIngredient of ingredient.extraIngredients) {
                const extraDoc = itemDoc.collection(EXTRA_INGREDIENTS).doc()
                const nItemExtraIngredient = {
                    id: extraIngredient.id,
                    localizedName: extraIngredient.localizedNames,
                    ingredientId: ingredient.id,
                    price: extraIngredient.price?.ammount,
                    selected: extraIngredient.selected,
                }
                batch.set(extraDoc, nItemExtraIngredient)
            }
        }
    }

    const itemDoc = await newOrder.collection(ITEMS).doc()
    if(type == TypeOrder.DELIVERY){
        const nItem = {
            name: "Servicio de Envío",
            quantity: 1,
            price: costService,
            details: "",
            localizedName: "Servicio de Envío",
            type: type
        }
        batch.set(itemDoc, nItem)
    }else if(type == TypeOrder.COLLECT){
        const nItem = {
            name: "Pasar a Recoger",
            quantity: 1,
            price: 0,
            details: "",
            localizedName: "Pasar a Recoger",
            type: type
        }
        batch.set(itemDoc, nItem)
    }
    batch.commit().then(() => {
        order.id = newOrder.id
        order.sent = true
    })
    return newOrder.id
}

async function saveFavorites() {
    const user = store.state.user
    if(user){
        try {
            if (!store.state.menu) { return null }
            const query = db.collection(USER).doc(user.id).collection(FAVORITES).doc(store.state.menu.id)
            const results = await query.get()
            const snapShot = results.data()
            if(snapShot != undefined){
                await query.update({
                    orderPlaced: snapShot.orderPlaced + 1,
                    active: true
                })
            }else{
                query.set({
                    orderPlaced: 1,
                    active: true
                })
            }
        } catch (error) {
            return null
        }
    }
}

async function sendOrder(newOrder: Order, token = "", type: string, costService?: number): Promise<string> {
    try {
        const newOrderId = await saveOrder(newOrder, token, type, costService)
        await saveFavorites()
        return newOrderId
    } catch (error) {
        console.log(error)
        return ''
    }
}

function compareItemExtraIngredients(AExtraIngredients: Array<ItemExtraIngredient>, BExtraIngredients: Array<ItemExtraIngredient>): boolean {
    if (AExtraIngredients.length != BExtraIngredients.length) {
        return false
    }

    for (const aExtraIngredient of AExtraIngredients) {
        let found = false
        for (const bExtraIngredient of BExtraIngredients) {
            if(bExtraIngredient.id == aExtraIngredient.id && bExtraIngredient.selected == aExtraIngredient.selected){
                found = true
            } 
        }
        if(!found) return false       
    }
    return true
}

function compareItemIngredients(AIngredients: Array<ItemIngredient>, BIngredients: Array<ItemIngredient>): boolean {
    if (AIngredients.length != BIngredients.length) {
        return false
    }

    for (const aIngredient of AIngredients) {
        let found = false
        for (const bIngredient of BIngredients) {
            if(bIngredient.id == aIngredient.id && bIngredient.selected == aIngredient.selected){
                found = compareItemExtraIngredients(aIngredient.extraIngredients, bIngredient.extraIngredients)                
            } 
        }
        if(!found) return false       
    }
    return true
}

function getIndexOfItem(itemToSearch: Item, items: Array<Item>) {
    for (const [itemIndex, itemToCompare] of items.entries() ) {

       //simple comparision
       let simple = false
       let complex = false
       
       if( itemToSearch.id == itemToCompare.id && itemToSearch.details == itemToCompare.details){
           simple = true
           complex = compareItemIngredients(itemToSearch.ingredients, itemToCompare.ingredients)

           //complex comparision
          if(simple && complex) return itemIndex
       }
    }
    return -1    
}

function generateReadableId(): string {
    if(store.state.menu){
        const menu: string = store.state.menu?.name
        const space = menu.indexOf(' ')
        const nameId = space === -1 ? menu : menu.substring(0, space)
        const dateNow = Date.now()
        return `${nameId}-${dateNow}`
    }else { 
        return Array(2).fill(0).reduce((p) => p + pad(getRandomInt()), '')
    }
}

export default {
    paymentExist,
    sendOrder,
    generateReadableId,
    getIndexOfItem
}
