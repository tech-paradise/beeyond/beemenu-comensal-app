import * as CollectionNames from "@/services/CollectionsNames";
import StorageService from "@/services/StorageService";
import firebase from "firebase";
import {DaysOfWeek, DeliveryMenu, Schedule, ScheduleFromFB} from "@/models/DeliveryMenu";
import {loadCollection} from "@/services/MenuService";
import db from "@/config/db";
import LocalizedText from "@/models/LocalizedText";

async function descriptionDeliveryFromDocument(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<LocalizedText> {
    const descriptionDelivery = new LocalizedText(firebaseDocument.id)
    const snapshot = firebaseDocument.data()
    if (snapshot != undefined) {
        if (snapshot.language !== undefined) descriptionDelivery.language = snapshot.language
        if (snapshot.text !== undefined) descriptionDelivery.text = snapshot.text
    }
    return descriptionDelivery
}

async  function  deliveryMenuFromSnapshot( firebaseDocument: firebase.firestore.DocumentSnapshot ): Promise<DeliveryMenu>{
    const newDeliveryMenu = new DeliveryMenu(firebaseDocument.id);
    const snapshot = firebaseDocument.data();
    if(snapshot){
        if (snapshot.address) newDeliveryMenu.address = snapshot.address;
        if (snapshot.costShipping) newDeliveryMenu.costShipping = snapshot.costShipping;
        if (snapshot.active) newDeliveryMenu.active = snapshot.active;
        if (snapshot.descriptionDeliveryDefault) newDeliveryMenu.descriptionDeliveryDefault = snapshot.descriptionDeliveryDefault;
        if (snapshot.imageDelivery) newDeliveryMenu.imageDelivery = snapshot.imageDelivery;
        if (snapshot.location) newDeliveryMenu.location = snapshot.location;
        if (snapshot.menuId) newDeliveryMenu.menuId = snapshot.menuId;
        if (snapshot.onlinePayment) newDeliveryMenu.onlinePayment = snapshot.onlinePayment;
        if (snapshot.payment) newDeliveryMenu.payment = snapshot.payment;
        if (snapshot.phones) newDeliveryMenu.phones = snapshot.phones;
        if (snapshot.restaurantId) newDeliveryMenu.restaurantId = snapshot.restaurantId;
        if (snapshot.collect) newDeliveryMenu.collect = snapshot.collect;
        if (snapshot.schedule) {

            const schedule = {} as Schedule;
            for (const day  in snapshot.schedule){
                const dayAux = day as DaysOfWeek;
                const daySchedule = (snapshot.schedule as ScheduleFromFB) [dayAux];
                schedule[dayAux] = {
                    initHour: daySchedule.initHour.toDate(),
                    initDay: daySchedule.initDay as DaysOfWeek,
                    endHour: daySchedule.endHour.toDate(),
                    endDay: daySchedule.endDay as DaysOfWeek,
                    open: daySchedule.open,
                }
            }
            newDeliveryMenu.schedule = schedule;
        }

        if (snapshot.shipping) newDeliveryMenu.shipping = snapshot.shipping;
    }

    newDeliveryMenu.descriptionDelivery = await loadCollection(
        firebaseDocument.ref.collection(CollectionNames.DESCRIPTIONDELIVERY),
        descriptionDeliveryFromDocument
    );

    newDeliveryMenu.descriptionEstimates = await loadCollection(
        firebaseDocument.ref.collection(CollectionNames.DESCRIPTIONESTIMATES),
        descriptionDeliveryFromDocument
    );

    newDeliveryMenu.imageDeliveryURL = (await StorageService.getFile(newDeliveryMenu.imageDelivery)) || '';

    return newDeliveryMenu;
}

export async function getDeliveryMenu(id: string): Promise<DeliveryMenu | undefined> {
    try {
        const results = await db
            .collection(CollectionNames.DELIVERIES)
            .doc(id)
            .get()
        if (!results.exists) {
            return undefined
        } else {
            return await deliveryMenuFromSnapshot(results)
        }
    } catch (error) {
        return undefined
    }
}

