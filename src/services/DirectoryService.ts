import db from '@/config/db'
import BusinessTags from '@/models/BusinessCategory'
import * as CollectionNames from './CollectionsNames'

async function tagsFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<BusinessTags> {
	const tag = new BusinessTags(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
        if (snapshot.id !== undefined) tag.id = snapshot.id
		if (snapshot.name !== undefined) tag.name = snapshot.name
        if (snapshot.active !== undefined) tag.active = snapshot.active
	}
	return tag
}

export async function getBusinessTags(): Promise<BusinessTags[] | undefined>{
    const results: BusinessTags [] = []
    try {
        const query = await db.collection(CollectionNames.BUSINESSTAGS).get()
        if(query.empty) return results
        Promise.all(query.docs.map(async d => results.push(await tagsFromSnapshot(d))))
        return results
    } catch (error) {
        console.log(error)
    }
}


export default {
	getBusinessTags
}
