import db from '@/config/db'
import firebase from 'firebase';
import StorageService from '@/services/StorageService'
import Menu, { TypeView } from '@/models/Menu'
import Category from '@/models/Category'
import Product from '@/models/Product'
import Ingredient from '@/models/Ingredient'
import LocalizedText from '@/models/LocalizedText'
import Localization from '@/models/Localization'
import Price from '@/models/Price'
import Review from '@/models/Review'
import SocialNetworks from '@/models/SocialNetworks'
import * as CollectionNames from './CollectionsNames'
import store from '@/store'
import Subcategory from '@/models/Subcategory'
import ExtraIngredient from '@/models/ExtraIngredient'
import { TypeOrder } from '@/store/state';

interface FirebaseWhere {
	attribute: string;
	operator: firebase.firestore.WhereFilterOp;
	value: string | boolean | number | Array<number | string | boolean>;
}

export async function loadCollection<T>(
	collectionReference: firebase.firestore.CollectionReference,
	constructor: Function,
	rules: Array<FirebaseWhere> = []
): Promise<T[]> {
	const results: T[] = []

	try {
		let collectionReferenceWithRules: firebase.firestore.Query = collectionReference
		rules.forEach(rule => {
			collectionReferenceWithRules = collectionReferenceWithRules.where(rule.attribute, rule.operator, rule.value)
		})

		const resultsReferences = await collectionReferenceWithRules.get()

		if (resultsReferences.empty) {
			return results
		}

		Promise.all(resultsReferences.docs.map(async d => results.push(await constructor(d))))
	} catch (e) {
		console.log(e)
	}

	return results
}

function priceFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Price {
	const price = new Price(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.ammount !== undefined) price.ammount = snapshot.ammount
		if (snapshot.currency !== undefined) price.currency = snapshot.currency
	}
	return price
}

function localizedTextFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): LocalizedText {
	const localizedText = new LocalizedText(firebaseDocument.id)
	const snapshot = firebaseDocument.data()

	if (snapshot != undefined) {
		if (snapshot.text !== undefined) localizedText.text = snapshot.text
		if (snapshot.language !== undefined) localizedText.language = snapshot.language
	}

	return localizedText
}

async function extraIngredientFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<ExtraIngredient> {
	const extraIngredient = new ExtraIngredient(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.name !== undefined) extraIngredient.name = snapshot.name
	}
	Promise.all([
		(extraIngredient.localizedNames = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZED_NAMES),
			localizedTextFromSnapshot
		)),
		(extraIngredient.prices = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.PRICES),
			priceFromSnapshot
		))
	])
	return extraIngredient
}

async function localizationFromDocument(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Localization> {
	const localization = new Localization(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.currency !== undefined) localization.currency = snapshot.currency
		if (snapshot.change !== undefined) localization.change = snapshot.change
		if (snapshot.language !== undefined) localization.language = snapshot.language
		if (snapshot.name !== undefined) localization.name = snapshot.name
		if (snapshot.icon !== undefined) localization.icon = snapshot.icon
		if (snapshot.order !== undefined) localization.order = snapshot.order
	}
	localization.iconURL = (await StorageService.getFile(localization.icon)) || ''
	return localization
}



async function reviewFromSnapshowt(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Review> {
	const review = new Review(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.content !== undefined) review.content = snapshot.content
		if (snapshot.language !== undefined) review.language = snapshot.language
		if (snapshot.name !== undefined) review.name = snapshot.name
		if (snapshot.positive !== undefined) review.positive = snapshot.positive
		if (snapshot.createdAt !== undefined)
			review.createdAt = (snapshot.createdAt as firebase.firestore.Timestamp).toDate()
	}
	return review
}

async function ingridientFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Ingredient> {
	const ingridient = new Ingredient(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.name !== undefined) ingridient.name = snapshot.name
	}

	ingridient.localizedNames = await loadCollection(
		firebaseDocument.ref.collection('localizedNames'),
		localizedTextFromSnapshot
	)

	return ingridient
}

async function getDifferentReviews(collection: firebase.firestore.CollectionReference) {
	const result = { good: 0, bad: 0, total: 0 }

	try {
		let goodReferences:  firebase.firestore.QuerySnapshot
		let badReferences: firebase.firestore.QuerySnapshot

		Promise.all([
			(goodReferences = await collection.where('approved', '==', true).where('positive', '==', true).get()),
			(badReferences = await collection.where('approved', '==', true).where('positive', '==', false).get()),
		])

		result.good = goodReferences.size
		result.bad = badReferences.size
    result.total = result.good + result.bad

	} catch (error) {
		console.log(error)
	}
	return result
}

async function productFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Product> {
	const product = new Product(firebaseDocument.id)
	const snapshot = firebaseDocument.data()

	if (snapshot != undefined) {
		if (snapshot.photos !== undefined) product.photos = snapshot.photos
		if (snapshot.name !== undefined) product.name = snapshot.name
		if (snapshot.categoryId !== undefined) product.categoryId = snapshot.categoryId
		if (snapshot.subCategoryId !== undefined) product.subCategoryId = snapshot.subCategoryId
		if (snapshot.chefRecommended !== undefined) product.chefRecommended = snapshot.chefRecommended
		if (snapshot.chefRecommendedPriority !== undefined) product.chefRecommended = snapshot.chefRecommended
		if (snapshot.description !== undefined) product.description = snapshot.description
		if (snapshot.mostSelled !== undefined) product.mostSelled = snapshot.mostSelled
		if (snapshot.mostSelledPriority !== undefined) product.mostSelledPriority = snapshot.mostSelledPriority
		if (snapshot.name !== undefined) product.name = snapshot.name
		if (snapshot.order !== undefined) product.order = snapshot.order
		if (snapshot.delivery !== undefined) product.delivery = snapshot.delivery
		if (snapshot.video !== undefined) product.video = snapshot.video
	}

	let goodReviews; let badReviews; let reviews;
	Promise.all([
		(product.prices = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.PRICES),
			priceFromSnapshot
		)),
		(product.ingredients = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.INGREDIENTS),
			ingridientFromSnapshot
		)),
		(product.localizedNames = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZED_NAMES),
			localizedTextFromSnapshot
		)),
		(product.localizedDescriptions = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZED_DESCRIPTIONS),
			localizedTextFromSnapshot
		)),
		({ good:goodReviews, bad:badReviews, total:reviews }  = await getDifferentReviews(
			firebaseDocument.ref.collection(CollectionNames.REVIEWS)
		)),
		(product.photosURL = await Promise.all(product.photos.slice(0, 7).map(async (i) => (
			await StorageService.getFile(i) || ''
			))
		)),
	])

	product.goodReviews = goodReviews
	product.badReviews = badReviews
	product.totalReviews = reviews
	return product
}

async function subcategoryFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Subcategory> {
	const subcategory = new Subcategory(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.name !== undefined) subcategory.name = snapshot.name
		if (snapshot.icon !== undefined) subcategory.icon = snapshot.icon
		if (snapshot.order !== undefined) subcategory.order = snapshot.order
	}

	Promise.all([
		(subcategory.localizedNames = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZED_NAMES),
			localizedTextFromSnapshot
		)),
		(subcategory.iconURL = (await StorageService.getFile(subcategory.icon)) || '')
	])
	return subcategory
}

async function categoryFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Category> {
	const category = new Category(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.name !== undefined) category.name = snapshot.name
		if (snapshot.icon !== undefined) category.icon = snapshot.icon
		if (snapshot.showsAtHome !== undefined) category.showsAtHome = snapshot.showsAtHome
		if (snapshot.showsAtHomePriority !== undefined) category.showsAtHomePriority = snapshot.showsAtHomePriority
		if (snapshot.order !== undefined) category.order = snapshot.order
	}

	Promise.all([
		(category.localizedNames = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZED_NAMES),
			localizedTextFromSnapshot
		)),
		(category.subcategories = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.SUBCATEGORIES),
			subcategoryFromSnapshot,
			[{ attribute: 'active', operator: '==', value: true }]
		)),
		(category.iconURL = (await StorageService.getFile(category.icon)) || '')
	])
	return category
}

async function ingredientFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Ingredient> {
	const ingredient = new Ingredient(firebaseDocument.id)
	const snapshot = firebaseDocument.data()
	if (snapshot != undefined) {
		if (snapshot.name !== undefined) ingredient.name = snapshot.name
		if (snapshot.maxAmount !== undefined) ingredient.maxAmount = snapshot.maxAmount
		if (snapshot.productId !== undefined) ingredient.productId = snapshot.productId
		if (snapshot.required !== undefined) ingredient.required = snapshot.required
		if (snapshot.selected !== undefined) ingredient.selected = snapshot.selected
	}

	Promise.all([
		(ingredient.localizedNames = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZED_NAMES),
			localizedTextFromSnapshot
		)),
		(ingredient.extraIngredients = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.EXTRA_INGREDIENTS),
			extraIngredientFromSnapshot
		))
	])
	return ingredient
}

async function menuFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<Menu> {

	const newMenu = new Menu(firebaseDocument.id)
	const snapshot = firebaseDocument.data()

	if (snapshot != undefined) {
		if (snapshot.restaurantId !== undefined) {
			newMenu.restaurantId = snapshot.restaurantId
		}
		if (snapshot.background !== undefined) newMenu.background = snapshot.background
		if (snapshot.logo !== undefined) newMenu.logo = snapshot.logo
		if (snapshot.description !== undefined) newMenu.description = snapshot.description
		if (snapshot.mainColor !== undefined) newMenu.mainColor = snapshot.mainColor
		if (snapshot.buttonTextColor !== undefined) newMenu.buttonTextColor = snapshot.buttonTextColor
		if (snapshot.showMostSelled !== undefined) newMenu.showMostSelled = snapshot.showMostSelled
		if (snapshot.showChefRecommended !== undefined) newMenu.showChefRecommended = snapshot.showChefRecommended
		if (snapshot.showReviews !== undefined) newMenu.showReviews = snapshot.showReviews
		if (snapshot.name !== undefined) newMenu.name = snapshot.name
		if (snapshot.active !== undefined) newMenu.active = snapshot.active
		if (snapshot.useLogin !== undefined) newMenu.useLogin = snapshot.useLogin
	}

	Promise.all([
		(newMenu.products = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.PRODUCTS),
			productFromSnapshot,
			[{ attribute: 'active', operator: '==', value: true }]
		)),
		(newMenu.categories = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.CATEGORIES),
			categoryFromSnapshot,
			[{ attribute: 'active', operator: '==', value: true }]
		)),
		(newMenu.localizations = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.LOCALIZATIONS),
			localizationFromDocument
		)),
		(newMenu.ingredients = await loadCollection(
			firebaseDocument.ref.collection(CollectionNames.INGREDIENTS),
			ingredientFromSnapshot
		)),
	])

	return newMenu
}

async function getCurrentMenuRef(): Promise<firebase.firestore.DocumentReference | null> {
	try {
		if (!store.state.menu) {
			return null
		}

		const results = await db
			.collection(CollectionNames.MENUS)
			.doc(store.state.menu.id)
			.get()

		if (!results.exists) {
			return null
		} else {
			return results.ref
		}
	} catch (error) {
		return null
	}
}

export async function getReviewsOfProduct(productId: string, typeOfSearch: string): Promise<Review[]> {
	const results: Review[] = []
	try {
		if (!store.state.menu) return results
		const searchRef = '/menus/' + store.state.menu.id + '/products/' + productId + '/' + CollectionNames.REVIEWS

		let query: firebase.firestore.Query = db.collection(searchRef).where('approved', '==', true)
		switch (typeOfSearch) {
			case 'positives':
				query = query.where('positive', '==', true)
				break
			case 'negatives':
				query = query.where('positive', '==', false)
				break
			default:
				break
		}

		const searchResults = await query.get()
		if (searchResults.empty) return results

		Promise.all(searchResults.docs.map(async d => results.push(await reviewFromSnapshowt(d))))
	} catch (error) {
		console.log(error)
	}

	return results
}

export async function sendReview(productId: string, review: Review, token = ""): Promise<boolean> {
  try {
		if (!store.state.menu) return false
		const searchRef = '/menus/' + store.state.menu.id + '/products/' + productId + '/' + CollectionNames.REVIEWS
    const firebaseDate = new firebase.firestore.Timestamp(review.createdAt.getTime()/1000, 0)
    
    await db.collection(searchRef).doc(review.id).set({
        name: review.name,
        language: review.language,
        content: review.content,
        positive: review.positive,
        approved: false,
        createdAt: firebaseDate,
        menuId: store.state.menu.id,
        productId: productId,
        captchaToken: token
    })

    return true
   } catch (error) {
    console.log(error) 
     return false
   } 
}

export async function addViewToProduct(productId: string, typeOrder: TypeOrder): Promise<boolean> {
  try {
		if (!store.state.menu) return false
		const searchRef = '/menus/' + store.state.menu.id + '/products/' + productId + '/' + CollectionNames.VIEWS
		const firebaseDate = new firebase.firestore.Timestamp((new Date()).getTime()/1000, 0)
	
		await db.collection(searchRef).add({
			createdAt: firebaseDate,
			productId: productId,
			menuId: store.state.menu.id,
			viewFrom: typeOrder
		})

		return true
   } catch (error) {
		console.log(error) 
		return false
   } 
}

export async function addViewToMenu(menuId: string): Promise<boolean> {
	try {
		const searchRef = '/menus/' + menuId + '/' + CollectionNames.VIEWS
		const firebaseDate = new firebase.firestore.Timestamp((new Date()).getTime()/1000, 0)

		await db.collection(searchRef).add({
			createdAt: firebaseDate
		})

		return true
	} catch (error) {
		console.log(error) 
		return false
	}
}

export async function addView(menuId: string, typeView: TypeView) {
	try {
		const result = await db
			.collection(CollectionNames.DELIVERIES)
			.doc(menuId)
			.get()

		const firebaseDate = new firebase.firestore.Timestamp((new Date()).getTime()/1000, 0)
		switch(typeView){
			case TypeView.COLLECT:
				result.ref.collection(CollectionNames.COLLECTVIEWS)
					.add({ createdAt: firebaseDate })
				break

			case TypeView.SHIPPING:
				result.ref.collection(CollectionNames.SHIPPINGVIEWS)
					.add({ createdAt: firebaseDate })
				break

			case TypeView.QR:
				result.ref.collection(CollectionNames.QRVIEWS)
					.add({ createdAt: firebaseDate })
				break
		}
		return true
	} catch (error) {
		console.log(error) 
		return false
	}
}

export async function userCommentedProduct(productId: string, userId: string): Promise<boolean>{
  try {
		if (!store.state.menu) return false
		const searchRef = '/menus/' + store.state.menu.id + '/products/' + productId + '/' + CollectionNames.REVIEWS
    
   const result = await db.collection(searchRef).doc(userId).get()
    
    return result.exists
   } catch (error) {
    console.log(error) 
     return false
   } 
}

export async function getMenu(id: string): Promise<Menu | null> {
	try {
		const results = await db
			.collection(CollectionNames.MENUS)
			.doc(id)
			.get()
		if (!results.exists) {
			return null
		} else {
			return await menuFromSnapshot(results)
		}
	} catch (error) {
		return null
	}
}


export async function getMenuIdFromRestaurant(id: string): Promise<string | null> {
	try {
		const results = await db
			.collection(CollectionNames.MENUS)
			.where('restaurantId', '==', id)
			.limit(1)
			.get()

		if (results.empty) {
			return null
		} else {
			return results.docs[0].id;
		}
	} catch (error) {
		return null
	}
}


export async function getSocialNetworkFromMenu(menuId: string): Promise<SocialNetworks | null> {
	try {
		const results = await db
			.collection(CollectionNames.SOCIALNETWORKS)
			.doc(menuId)
			.get()

		if (!results.exists) {
			return null
		} else {

			const newSocialNetworks = new SocialNetworks
			const snapshot = results.data()
			if (snapshot != undefined) {
				if (snapshot.facebook !== undefined) newSocialNetworks.facebook = snapshot.facebook
				if (snapshot.instagram !== undefined) newSocialNetworks.instagram = snapshot.instagram
			}
			return newSocialNetworks
		}
	} catch (error) {
		return null
	}
}

export default {
	getMenu,
	getMenuIdFromRestaurant,
	addViewToProduct,
	addViewToMenu,
	addView,
	getReviewsOfProduct,
	sendReview,
	userCommentedProduct,
	getCurrentMenuRef,
	getSocialNetworkFromMenu,
}
