import db from "@/config/db";
import Restaurant from '@/models/Restaurant'
import {RESTAURANTS, SLUGS} from './CollectionsNames';
import * as firebase from 'firebase/app';
import {Slug} from "@/models/Slug";


function restaurantFromSnapshot(snapshot: firebase.firestore.DocumentSnapshot): Restaurant {
    const newRestaurant = new Restaurant(snapshot.id)
    const data = snapshot.data()
    if(data){
        if (data.receiveOrders !== undefined) newRestaurant.receiveOrders = data.receiveOrders;
    }
    return newRestaurant;
}

const slugFromSnapshot = (snapshot: firebase.firestore.DocumentSnapshot): Slug | undefined => {
    const data = snapshot.data();
    return data
        ? new Slug(snapshot.id,
            data.restaurantId,
            data.menusSlugs
                ? new Map(Object.entries(data.menusSlugs))
                : new Map()
        )
        : undefined;
}

export async function getRestaurantById(id: string): Promise<Restaurant | null> {
    try {
        const results = await db
            .collection(RESTAURANTS).doc(id)
            .get();
        if (results.exists) {
            return restaurantFromSnapshot(results)
        }
    } catch (error) {
        console.log(error)
    }
    return null
}

export async function getSlugById(slug: string): Promise<Slug | undefined> {

    const results = await db
        .collection(SLUGS).doc(slug)
        .get();

    return slugFromSnapshot(results);
}

export async function getSlugByRestaurantId(restaurantId: string): Promise<Slug | undefined> {

    const results = await db
        .collection(SLUGS).where('restaurantId','==',restaurantId)
        .get();

    return results.empty
        ? undefined
        : slugFromSnapshot(results.docs[0]);

}
