//First Collections
export const RESTAURANTS = "restaurants"
export const MENUS = "menus"
export const ORDERS = "orders"
export const SOCIALNETWORKS = "socialNetworks"
export const DELIVERIES = "deliveries"
export const SLUGS = "slugs"
export const USER = 'users'
export const PAYMENTS = 'payments'
export const BUSINESSTAGS = 'businessTags'

//Second Collections
export const CATEGORIES = "categories"
export const SUBCATEGORIES = "subcategories"
export const LOCALIZATIONS = 'localizations'
export const PRODUCTS = "products"
export const ITEMS = "items"
export const SHIPPINGVIEWS = "shippingviews"
export const COLLECTVIEWS = "collectviews"
export const QRVIEWS = "qrviews"
export const DESCRIPTIONDELIVERY = "descriptionDelivery"
export const DESCRIPTIONESTIMATES = "descriptionEstimates"
export const ADDRESSBOOK = 'addressBook'
export const INVOICING = 'invoicing'
export const FAVORITES = 'favorites'

//Third Collections
export const LOCALIZED_NAMES = 'localizedNames'
export const EXTRA_INGREDIENTS = 'extraIngredients'
export const LOCALIZED_DESCRIPTIONS = 'localizedDescriptions'
export const PRICES = 'prices'
export const INGREDIENTS = 'ingredients'
export const REVIEWS = 'reviews'
export const LIKES = 'likes'
export const VIEWS = 'views'
