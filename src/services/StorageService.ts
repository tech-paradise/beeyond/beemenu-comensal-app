import storage from "@/config/storage";

export async function getFile(filePath: string | null): Promise<string | null> {
    if (filePath == null || filePath == "") {
        return null
    }

    try {
        return await storage.ref('/').child(filePath)
            .getDownloadURL();
    } catch (error) {
        console.log("error", error);
        return null;
    }
}

export default {
    getFile,
}