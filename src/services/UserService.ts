import {User} from "firebase";
import db from "@/config/db";
import {USER, ADDRESSBOOK, INVOICING} from "@/services/CollectionsNames";
import {User as UserModel} from "../models/User";
import InvoiceData from "@/models/InvoiceData";
import firebase from 'firebase';

export async function getUser(userAuth: User): Promise <UserModel | null> {
    if (userAuth){
        const user = await db.collection(USER).doc(userAuth.uid).get();
        if (user.exists){
            const data = user.data();
            return new UserModel(user.id, data?.name || '' , data?.email || '', data?.phone || '', await userAuth.getIdToken(true) || '');
        }

        await db.collection(USER).doc(userAuth.uid).set({
            name: userAuth.displayName || '',
            email: userAuth.email || '',
            phone: userAuth.phoneNumber || '',
            isUser: true,
            createdAt: firebase.firestore.FieldValue.serverTimestamp()
        });

        return new UserModel(userAuth.uid, userAuth.displayName || '', userAuth.email || '', userAuth.phoneNumber || '', await userAuth.getIdToken(true) || '')
    }
    return null;
}

export async function updateUser(user: UserModel, name: string, email: string, phone: string, token?: string): Promise <UserModel> {
    await db.collection(USER).doc(user.id).update({
        email: email,
        name: name,
        phone: phone
    })

    return new UserModel(user.id, name, email, phone, token)
}

async function InvoicingFromSnapshot(firebaseDocument: firebase.firestore.DocumentSnapshot): Promise<InvoiceData> {

    const newInvoiceData = new InvoiceData(firebaseDocument.id)
    const snapshotInvoice = firebaseDocument.data()

    if (snapshotInvoice != undefined) {
        if (snapshotInvoice.idUser !== undefined) {newInvoiceData.idUser = snapshotInvoice.idUser}
        if (snapshotInvoice.name !== undefined) newInvoiceData.name = snapshotInvoice.name
        if (snapshotInvoice.email !== undefined) newInvoiceData.email = snapshotInvoice.email
        if (snapshotInvoice.phoneNumber !== undefined) newInvoiceData.phoneNumber = snapshotInvoice.phoneNumber
        if (snapshotInvoice.rfc !== undefined) newInvoiceData.rfc = snapshotInvoice.rfc
        if (snapshotInvoice.businessName !== undefined) newInvoiceData.businessName = snapshotInvoice.businessName
        if (snapshotInvoice.address !== undefined) newInvoiceData.address = snapshotInvoice.address
        if (snapshotInvoice.extNumber !== undefined) newInvoiceData.extNumber = snapshotInvoice.extNumber
        if (snapshotInvoice.intNumber !== undefined) newInvoiceData.intNumber = snapshotInvoice.intNumber
        if (snapshotInvoice.postalCode !== undefined) newInvoiceData.postalCode = snapshotInvoice.postalCode
        if (snapshotInvoice.state !== undefined) newInvoiceData.state = snapshotInvoice.state
        if (snapshotInvoice.locality !== undefined) newInvoiceData.locality = snapshotInvoice.locality
        if (snapshotInvoice.region !== undefined) newInvoiceData.region = snapshotInvoice.region
    }
    return newInvoiceData
}

export async function saveInvoincing(invoicing: InvoiceData, idUser: string){

    const results = await db.collection(USER).doc(idUser)
    
    return results.collection(INVOICING).add({
        idUser: invoicing.idUser,
        name: invoicing.name,
        email: invoicing.email,
        phoneNumber: invoicing.phoneNumber,
        rfc: invoicing.rfc,
        businessName: invoicing.businessName,
        address: invoicing.address,
        extNumber: invoicing.extNumber,
        intNumber: invoicing.intNumber,
        postalCode: invoicing.postalCode,
        state: invoicing.state,
        locality: invoicing.locality,
        region: invoicing.region
    })
}

export async function getInvoiceData(idUser: string): Promise<InvoiceData | null> {
    try {
        const results = await db.collection(USER).doc(idUser).collection(INVOICING).get()
        if(results.empty){
            return null
        }else{
            return await InvoicingFromSnapshot(results.docs[0])
        } 
    } catch (error) {
        return null
    } 
}

export async function updateInvoicing(invoicing: InvoiceData, userId: string) {

    const results = await db.collection('users').doc(userId)

    return results.collection(INVOICING).doc(invoicing.id).update({
        idUser: invoicing.idUser,
        name: invoicing.name,
        email: invoicing.email,
        phoneNumber: invoicing.phoneNumber,
        rfc: invoicing.rfc,
        businessName: invoicing.businessName,
        address: invoicing.address,
        extNumber: invoicing.extNumber,
        intNumber: invoicing.intNumber,
        postalCode: invoicing.postalCode,
        state: invoicing.state,
        locality: invoicing.locality,
        region: invoicing.region
    })

}

export default{
    saveInvoincing,
    getInvoiceData,
    updateInvoicing,
}
