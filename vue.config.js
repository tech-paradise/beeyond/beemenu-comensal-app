module.exports = {
    runtimeCompiler: true,
    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: true
        }
    },
    pwa: {
        name: 'BeeMenu',
        display: 'standalone',
        themeColor: '#000',
        appleMobileWebAppCapable: 'yes',
        manifestPath: '/public/manifest.json'
      }
}
